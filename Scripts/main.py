#!/usr/bin/python

import RPi.GPIO as GPIO #pins
import datetime		#time
import os		#file
import time		#duh
from smbus import SMBus	#I2C
import Adafruit_DHT	#DHT
import csv		#CSV output
import socket		#TCP output
import errno		#error handling

### Variables

#water variables
mThresh = 210 #water the soil when it gets this dry
waterFor = 45 #seconds

#Number of minutes between readings
waitFor = 5 #minutes

#Server info
HOST = '192.168.1.101'
PORT = 12345

#Timestamp format
format = "%Y-%m-%d %H:%M:%S"

#file info
offlineFilePath = '/greenhouse/offline.buffer'
buff = open(offlineFilePath,'a+')

csvFilePath = '/greenhouse/zeus.csv'
csvf = open(csvFilePath,'a')

filePath = '/greenhouse/prometheus.log'
f = open(filePath,'a')

waterFilePath = '/greenhouse/water.log'
watf = open(waterFilePath,'a')

#Without this, it gives warnings about pin usage
GPIO.setwarnings(False)

#pins
DHTpin = 4
lightPin = 22
waterPin = 27
soilPowerPin = 17

#Names pins for BCM, not BOARD
GPIO.setmode(GPIO.BCM)

#pin mode setup
GPIO.setup(lightPin,GPIO.OUT)
GPIO.setup(waterPin,GPIO.OUT)
GPIO.setup(soilPowerPin,GPIO.OUT)

#This code is based on Adafruit's standard code
def DHT():
	#Get reading from DHT11
	humidity, temperature = Adafruit_DHT.read_retry(Adafruit_DHT.DHT22, DHTpin)

	if humidity is not None and temperature is not None:
#        	print 'Temp={0:0.1f}*C  Humidity={1:0.1f}%'.format(temperature, humidity)
#		s = 'Temp={0:0.1f}*C  Humidity={1:0.1f}%\n'.format(temperature, humidity)
		#f.write(str(s))
		return humidity, temperature
	else:
        	print 'Failed to get reading. Try again!'


#Get voltage reading from soil resistence tester
def soil(pin):
	#send power to soil sensor
	GPIO.output(pin,True)

	#Read a value from analogue input 0 
	#in A/D in the PCF8591P @ address 0x48
	bus = SMBus(1)

	bus.write_byte(0x48, 0) # set control register to read channel 0
	whatevs = bus.read_byte(0x48) #first read is a junk read
	reading = bus.read_byte(0x48) # read A/D
#	print(reading)

	#Turn off soil sensor to prevent electrolysis
	GPIO.output(pin,False)

	#Convert from 3.3v reading to 5v reading to match legacy data
	reading = reading * 5 / 3.3

	return reading

#outputs to zeus.csv
def writeCSV(time,temp,humidity,soil):
	row = [time,CtoF(temp),humidity,soil]
	csv.writer(csvf).writerow(row)
	return	

#outputs to prometheus.log
def humanRead(time,temp,humidity,soil):
	f.write('{}\n'.format(time))
	f.write('Temperature: {0:0.1f}*F\n'.format(CtoF(temp)))
	f.write('Humidity: {0:0.1f}%\n'.format(humidity))
	f.write('Soil moisture: {}\n'.format(soil))

#output to water log
def waterLog(time):
	watf.write('{}\n'.format(time))

#function-ated for clarity
def getTime():
	return datetime.datetime.today().strftime(format)

#print to screen
def screenOutput(time,temp,humidity,soil):
	print('{}'.format(time))
        print('Temperature: {0:0.1f}*F'.format(CtoF(temp)))
        print('Humidity: {0:0.1f}%'.format(humidity))
        print('Soil moisture: {}'.format(soil))
	return

#Convert Celcius to Ferinheight
def CtoF(C):
	F=C*9/5 +32
	return F

#check to see if the soil needs watered
def soilCheck(soil,mThresh,waterFor):
	if soil < mThresh:
		f.write('Pump Activated')
		print('Pumping!')
		water(waterFor)

#turn on the pump relay for X seconds
def water(waterFor):
	GPIO.output(waterPin,True)
	time.sleep(waterFor)
	GPIO.output(waterPin,False)
	
	#log the time
	waterLog(getTime())

def SSLout(time,temp,humidity,soil,HOST,PORT):

	row = [time, CtoF(temp),humidity,soil]
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR,1)
	sslSocket = socket.ssl(s)	
	#try connection
	try:
        	s.connect((HOST, PORT))
	#handle offline error
	except socket.error as e:
		print('Socket Error# {}'.format(e.errno))
               	print('OFFLINE MODE')
               	csv.writer(buff).writerow(row)
               	return
       	#	elif e.errno == errno.ECONNREFUSED:
  	 #       	#print to buffer
	#		print('Connection refused: OFFLINE MODE')
	#		csv.writer(buff).writerow(row)
	#		return
#	print('else')
	#check if buffer is empty
	buff.seek(0)
	if buff.read(1) == '':
			#just send it
		print('SSL OK')
		sslSocket.write('{},{},{},{}\n'.format(time,CtoF(temp),humidity,soil))
		sslSocket.write('Done!')

	else:
		#send the buffer, then the current reading, delete buffer
		print('Connection successful! Sending buffer')
		
		#Send each line
		for line in buff:
			#send
#			print(line)
			sslSocket.write(str(line))

		#Send current line and OK
		sslSocket.write('{},{},{},{}\n'.format(time,CtoF(temp),humidity,soil))
		sslSocket.write('Done!')

		#delete buffer
		buff.seek(0)
		buff.truncate()
		
		#close connection

##Start Main()
while 1 == 1:
		#get sensor values	
	thetime = getTime()
	humidity, temperature = DHT()
	soilRead = soil(soilPowerPin)

		#output to various files
	writeCSV(thetime,temperature,humidity,soilRead)	
	humanRead(thetime,temperature,humidity,soilRead)
	screenOutput(thetime,temperature,humidity,soilRead)	
	SSLout(thetime,temperature,humidity,soilRead,HOST,PORT)

		#Water if needed
	soilCheck(soilRead,mThresh,waterFor)
		
		#add space to make output look good
	print('')

		#Update file by flushing write buffer
	f.flush()
	os.fsync(f.fileno())

		#wait 
	time.sleep(waitFor*60)


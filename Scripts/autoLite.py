#!/usr/bin/python

import time
import RPi.GPIO as GPIO

#Pins BCM style
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
lightPin = 22

#if time is before 6 and after 10pm, turn on the light
if time.localtime().tm_hour >= 6 and time.localtime().tm_hour < 22 :
	GPIO.setup(lightPin,GPIO.OUT)
	GPIO.output(lightPin,1)
	print 'It\'s daytime!'
else:
	print 'It\'s nighttime!'

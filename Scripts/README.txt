These scripts are written in python and commented, so you should be able to get a good idea of what each script is doing without much programming knowledge.

The autoLite.py script checks what time it is during bootup to determine if the lighting should be on or off. This is nessecary because the lighting is controlled by a crontab so without it, the light would need to be manually turned on or you would lose the rest of the day's lighting.

main.py , in a nutshell, reads the data from the sensors, records the data in various formats and sends it to the server over SSL and runs a control loop for the soil moisture. The algorithm for when the soil is "dry" is very basic. If the reading from the soil sensor is below a threshold, it will run the water() function to water the garden.


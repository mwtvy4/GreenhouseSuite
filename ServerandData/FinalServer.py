import os
import socket
import time
from OpenSSL import SSL

#port info
PORT = 12345

#file info
filepath = 'hera.csv'
f = open(filepath,'a')

temperaturePath = '/Library/WebServer/Documents/temperature.csv'
tCSV = open(temperaturePath,'a+b')

humidityPath = '/Library/WebServer/Documents/humidity.csv'
hCSV = open(humidityPath,'a+b')

soilPath = '/Library/WebServer/Documents/soil.csv'
sCSV = open(soilPath,'a+b')

context = SSL.Context(SSL.SSLv23_METHOD)
context.use_privatekey_file('greenhouse.key')
context.use_certificate_file('greenhouse.crt')

print('Starting Greenhouse server...')
while True:
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

	s = SSL.Connection(context, s)

	s.bind(('', PORT))
	s.listen(5)

	(connection, address) = s.accept()
#connection.send('groovy')
	
	data = connection.recv(1024)
	while data <> 'Done!':
	#print the reading
		print(data.rstrip('\n'))
	#write to file
		f.write(data)
	#split into each reading
		splot = data.split(',')
	#write data to split files
		tCSV.write('{},{}\n'.format(splot[0],splot[1]))
		hCSV.write('{},{}\n'.format(splot[0],splot[2]))
		sCSV.write('{},{}\n'.format(splot[0],splot[3]))
	#read next line
		data = connection.recv(1024)

#refresh the files
	f.flush()
	tCSV.flush()
	hCSV.flush()
	sCSV.flush()
	os.fsync(f.fileno())
	os.fsync(tCSV.fileno())
	os.fsync(hCSV.fileno())
	os.fsync(sCSV.fileno())
#close socket
	s.close
	print('socket closed')

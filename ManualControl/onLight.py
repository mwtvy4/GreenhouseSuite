import RPi.GPIO as GPIO
import time
import smbus
import Adafruit_DHT

#Pins BCM style
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

DHTpin = 4
lightPin = 22
waterPin = 27

GPIO.setup(lightPin,GPIO.OUT)
GPIO.output(lightPin,False)
GPIO.setup(waterPin,GPIO.OUT)
GPIO.output(waterPin,False)

#while 1 == 1:
GPIO.output(lightPin,1)
#	GPIO.output(waterPin,0)


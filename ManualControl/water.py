import RPi.GPIO as GPIO
import time
import smbus
import Adafruit_DHT

#Pins BCM style
GPIO.setmode(GPIO.BCM)

DHTpin = 4
lightPin = 22
waterPin = 27

GPIO.setup(lightPin,GPIO.OUT)
#GPIO.output(lightPin,False)
GPIO.setup(waterPin,GPIO.OUT)
#GPIO.output(waterPin,False)

print("Watering for 15 sec")
GPIO.output(waterPin,1)
time.sleep(15)
GPIO.output(waterPin,0)


Thanks for taking a look at my project!

The automated greenhouse uses sensors and microcontrollers to bring a small garden into the big data age. Not only is the data visible in neat graphs on the server's website, the garden will sense if the soil is too dry and water itself!


This folder is organized into the main parts of the greenhouse system
The scripts are run on the Raspberry Pi on startup using init.d
There are small scripts in the manual control that are useful for debugging and demonstrations
The Pictures show the basic layout of the parts
And Sever and Data contains the logs and serverside scripts for communications and graphing

If you have any questions about the system, I would be happy to talk about my baby! Email me at MWT@sbcGlobal.net
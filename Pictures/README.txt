The heart of this project is the Raspberry Pi, a microcontroller that runs a Linux kernel, giving it tremendous capability. It has i/o pins for making readings and controlling the power plug, but needs an analog to digital converter to read the soil moisture sensor.
The humidity and air temperature sensor is a DHT22, which has a one wire digital interface.
To read the voltage of the soil sensor, I wired a i2c bus ADC, PCF8591, to the RPi.
The plug is digitally controlled. Starting with an i/o pin for each outlet, the pin drives a transistor, which lights up the led and switches the solid state relay controlling the 120v power.

